﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class ObjectiveQuestionnairesTab : BasicPageSetup
    {
        private By objectiveTabLink = By.LinkText("Objective");
        private By questionnaireSelect = By.Id("OutcomesQuestionaryFilter");
		const string DHI = "T526";
		const string HOOS = "T745";
		const string KOOS = "T737";
		const string LEFS = "T377";
		const string OSWESTRY = "T379";
		const string NDI = "T378";
		const string UEQD = "T415";
        const string FALLS = "T751";

		public ObjectiveQuestionnairesTab(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        private void SwitchToObjective()
        {
            ClickOnElement(objectiveTabLink);
        }

        private void SelectQuestionnaire(string testId)
        {
            SelectPopulatedListByValue(questionnaireSelect, testId);
        }

        private void ClickOnTest(string testId)
        {
            ClickOnElement(By.Id(testId + "_ShowTest"));
        }

        private void GetRidOfPendo()
        {
            ReadOnlyCollection<IWebElement> pendos = driver.FindElements(By.CssSelector("*[id^='_pendo']"));
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            if (pendos.Count > 0)
            {
                foreach (var pendo in pendos)
                {
                    js.ExecuteScript("arguments[0].style.display='none'", pendo);
                }
            }
        }

        public void SelectTest()
        {
            GetRidOfPendo();

            SwitchToObjective();

            SelectQuestionnaire(DHI);
            ClickOnTest(DHI);
            DhiTestPopup dhiTest = new DhiTestPopup(driver);
            dhiTest.CompleteTest();

			SelectQuestionnaire(KOOS);
			ClickOnTest(KOOS);
			KoosTestPopup koosTest = new KoosTestPopup(driver);
			koosTest.CompleteTest();

			SelectQuestionnaire(HOOS);
			ClickOnTest(HOOS);
			HoosTest hoosTest = new HoosTest(driver);
			hoosTest.CompleteTest();

			SelectQuestionnaire(LEFS);
            ClickOnTest(LEFS);
            LefsTest lefsTest = new LefsTest(driver);
            lefsTest.CompleteTest();

			SelectQuestionnaire(FALLS);
			ClickOnTest(FALLS);
			ModifiedFalls fallsTest = new ModifiedFalls(driver);
			fallsTest.CompleteTest();

			SelectQuestionnaire(OSWESTRY);
            ClickOnTest(OSWESTRY);
            OswestryTest oswestry = new OswestryTest(driver);
            oswestry.CompleteTest();

            SelectQuestionnaire(NDI);
            ClickOnTest(NDI);
            NeckDisabilityTest neckTest = new NeckDisabilityTest(driver);
            neckTest.CompleteTest();

			SelectQuestionnaire(UEQD);
			ClickOnTest(UEQD);
			QuickDash upperExtremity = new QuickDash(driver);
			upperExtremity.CompleteTest();
		}
    }
}
