﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class MfesOutcomes : BasicOutcomes
    {
        public MfesOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        public void FillTest()
        {
            FillTest(14, 11);
            SubmitTest();
        }
    }
}
