﻿using System;
using System.IO;

namespace InitialExamination
{
    public class FileAccess
    {
        private string path;

        public FileAccess(string configPath = null)
        {
            if (configPath == null)
            {
                path = Directory.GetCurrentDirectory() + "/patientdata.txt";
            } else
            {
                path = configPath + "/patientdata.txt";
            }
        }

        public string[] GetValues()
        {
            string[] values = File.ReadAllLines(path);
            for (int i = 0; i < values.Length; i++)
            {
                values[i] = values[i].Split('=')[1];
            }
            return values;
        }
    }
}
