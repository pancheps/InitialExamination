﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class OswestryOutcomes :BasicOutcomes
    {
        public OswestryOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        public void FillTest()
        {
            FillTest(10, 6);
            SubmitTest();
        }
    }
}
