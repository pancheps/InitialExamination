﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class SelectUserPage : BasicPageSetup
    {
        private By memberName;

        public SelectUserPage(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        public void SelectUser(string name)
        {
            memberName = By.PartialLinkText(name);
            ClickOnElement(memberName);
        }
    }
}
