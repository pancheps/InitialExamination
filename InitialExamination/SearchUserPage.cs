﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace InitialExamination
{
    public class SearchUserPage : BasicPageSetup
    {
        private By searchCriterion = By.Id("quick-search-criteria");
        private By searchUser = By.Id("quick-search");
        
        public SearchUserPage(IWebDriver driver) : base(driver)
        {
        }

        private void SelectCriterion(string loginText)
        {
            SelectPopulatedListByValue(searchCriterion, loginText);
        }

        private void SearchUser(string userText)
        {
            CompleteText(searchUser, userText);
        }

        private void SubmitForm()
        {
            IJavaScriptExecutor submitForm = (IJavaScriptExecutor)driver;
            submitForm.ExecuteScript("arguments[0].parentNode.submit();", driver.FindElement(searchUser));
        }

        public void Search(string userNameText)
        {
            SelectCriterion("user-name");
            SearchUser(userNameText);
            SubmitForm();
        }
    }
}
