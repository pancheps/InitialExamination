﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;
using OpenQA.Selenium.Support.UI;
using OpenQA.Selenium.Interactions;

namespace InitialExamination
{
    public class Utilities
    {
        public static int GetRandNumber(int start, int end)
		{
            Random r = new Random();
            return r.Next(start, end);
		}

		public static string GetOneItem(string commaText)
		{
			string[] stringElements = commaText.Split(',');
			Random randomNumber = new Random();
			int c = randomNumber.Next(0, stringElements.Length);
			string a = stringElements[c];
			return stringElements[randomNumber.Next(0, stringElements.Length)];
		}
	}
}
