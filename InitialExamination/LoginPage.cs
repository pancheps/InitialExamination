﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class LoginPage : BasicPageSetup
    {
        private By userLogin = By.Id("login-username");
        private By userPassword = By.Id("login-password");
        private By loginButton = By.Id("login-button");
		
        public LoginPage(IWebDriver receivedDriver) : base(receivedDriver)
        {
        }

        private void SetLogin(string loginText)
		{
            CompleteText(userLogin, loginText);
		}

        private void SetPassword(string passwordText)
		{
            CompleteText(userPassword, passwordText);
		}

        private void ClickLogin()
        {
            ClickOnElement(loginButton);
        }

        public void Login(string username, string password)
        {
            SetLogin(username);
            SetPassword(password);
            ClickLogin();
        }
    }
}
