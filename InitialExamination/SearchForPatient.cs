﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class SearchForPatient : BasicPageSetup
    {
        private By searchField = By.Name("SearchData");
        private By searchButton = By.ClassName("input2");

        public SearchForPatient(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        private void FillSearchPatient()
        {
            string[] data = new FileAccess().GetValues();
            CompleteText(searchField, data[0] + " " + data[1]);
        }

        private void ClickOnSearch()
        {
            ClickOnElement(searchButton);
        }

        public void SearchPatient()
        {
            FillSearchPatient();
            ClickOnSearch();
        }
    }
}
