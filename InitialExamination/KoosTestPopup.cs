﻿using System;
using System.Threading;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class KoosTestPopup : BasicPageSetup
    {
        private By patientSatistaction = By.Id("1SectionT7371-0");
        private By inputElements;
        private string testId = "T737";

        public KoosTestPopup(IWebDriver mainDriver) : base(mainDriver)
        {
            inputElements = By.CssSelector("input[id^='" + testId + "'][value='0']");
        }

        private void FillRadios()
        {
            ClickOnElement(patientSatistaction);
			ReadOnlyCollection<IWebElement> inputs = driver.FindElements(inputElements);
			foreach (var inputEl in inputs)
			{
				inputEl.Click();
			}
            driver.FindElement(By.Id(testId + "Symptoms020")).Click();
			driver.FindElement(By.Id(testId + "Symptoms030")).Click();
			driver.FindElement(By.Id(testId + "Symptoms040")).Click();
			driver.FindElement(By.Id(testId + "Symptoms050")).Click();
			driver.FindElement(By.Id(testId + "YN1")).Click();
		}

        private void ClickOk()
        {
			ClickOnElement(By.CssSelector("div#OriginalDiv" + testId + " div.functionaltest-calc-total input"));
		}

        public void CompleteTest()
        {
            FillRadios();
            ClickOk();
        }
    }
}
