﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace InitialExamination
{
    public class BasicPageSetup
    {
        protected IWebDriver driver;
        protected By selectedElementClass = By.ClassName("x-combo-selected");

        public BasicPageSetup(IWebDriver mainDriver)
        {
            driver = mainDriver;
        }

		protected void CompleteText(By protoElement, string text)
		{
			IWebElement element = driver.FindElement(protoElement);
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromMinutes(2));
			waitForElement.Until(ExpectedConditions.ElementToBeClickable(element));
			element.Clear();
			element.SendKeys(text);
		}

		protected void SelectPopulatedListByValue(By protoElement, string text)
		{
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
            wait.Until(ExpectedConditions.ElementExists(protoElement));
			SelectElement element = new SelectElement(driver.FindElement(protoElement));
			element.SelectByValue(text);
		}

		protected void SelectPopulatedListByText(By protoElement, string text)
		{
			SelectElement element = new SelectElement(driver.FindElement(protoElement));
			element.SelectByText(text);
		}

		protected void ClickOnElement(By protoElement)
		{
			IWebElement element = driver.FindElement(protoElement);
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromMinutes(2));
			waitForElement.Until(ExpectedConditions.ElementToBeClickable(element));
			element.Click();
		}

		protected void ClickOnLastCollectionElement(By protoElements)
		{
			ReadOnlyCollection<IWebElement> elements = driver.FindElements(protoElements);
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
			waitForElement.Until(ExpectedConditions.ElementToBeClickable(elements[elements.Count - 1]));
			elements[elements.Count - 1].Click();
		}

		protected void ClickOnFirstCollectionElement(By protoElements)
		{
			ReadOnlyCollection<IWebElement> elements = driver.FindElements(protoElements);
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(20));
			waitForElement.Until(ExpectedConditions.ElementToBeClickable(elements[elements.Count - 1]));
			elements[0].Click();
		}

		protected void SelectOnlyItemOnList(By protoElement)
		{
			driver.FindElement(protoElement).SendKeys(Keys.Command);
			var elements = driver.FindElements(selectedElementClass);
			IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
			js.ExecuteScript("arguments[0].click();", elements[elements.Count - 1]);
		}

		protected void WaitForLastOneVisibility()
		{
			ReadOnlyCollection<IWebElement> elements = driver.FindElements(selectedElementClass);
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
			waitForElement.Until(ExpectedConditions.ElementToBeClickable(elements[elements.Count - 1]));
		}

		protected void WaitForVisibility(By protoElement)
		{
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
			waitForElement.Until(ExpectedConditions.ElementIsVisible(protoElement));
		}

		protected void WaitForElementToExist(By protoElement)
		{
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
            waitForElement.Until(ExpectedConditions.ElementExists(protoElement));
		}

		protected void WaitForClickability(By protoElement)
		{
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
			waitForElement.Until(ExpectedConditions.ElementToBeClickable(protoElement));
		}

		protected void WaitForElementToDisappear(By protoElement)
		{
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
			waitForElement.Until(ExpectedConditions.InvisibilityOfElementLocated(protoElement));
		}

		protected void WaitForElementToHaveText(By protoElement, string text)
		{
			WebDriverWait waitForElement = new WebDriverWait(driver, TimeSpan.FromSeconds(40));
			waitForElement.Until(ExpectedConditions.TextToBePresentInElementLocated(protoElement, text));
		}
	}
}
