﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class DashOutcomes : BasicOutcomes
    {
        public DashOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        public void FillTest()
        {
            FillTest(11);
            SubmitTest();
        }
    }
}
