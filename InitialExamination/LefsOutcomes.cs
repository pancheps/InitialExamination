﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class LefsOutcomes : BasicOutcomes
    {
        public LefsOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
		}

		public void FillTest()
		{
            FillTest(20);
            SubmitTest();
		}
	}
}
