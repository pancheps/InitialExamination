﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class PatientDashboard : BasicPageSetup
    {
        private By recordActions = By.ClassName("ta--patient-chart--actions-select");
        private By submitButton = By.Id("prActionsSubmitButton");

        public PatientDashboard(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        private void SelectInitialExam()
        {
            SelectPopulatedListByText(recordActions, "Add Initial Examination");
        }

        private void SelectOutcomes()
        {
            SelectPopulatedListByText(recordActions, "Add Outcome");
        }

        private void ClickOnSubmit()
        {
            ClickOnElement(submitButton);
        }

        public void StartInitialExam()
        {
            SelectInitialExam();
            ClickOnSubmit();
        }
    }
}
