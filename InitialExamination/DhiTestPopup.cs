﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class DhiTestPopup : BasicPageSetup
    {
        private By patientSatisfaction;
        private By intakeCheckbox;
        private string testId = "T526";

        public DhiTestPopup(IWebDriver mainDriver, int? i = null) : base(mainDriver)
        {
			if (i == null)
			{
				patientSatisfaction = By.Id("1SectionT5261-0");
                intakeCheckbox = By.Id("3SectionT52626-0");
			}
			else
			{
				patientSatisfaction = By.Id("1SectionT5261-" + Utilities.GetRandNumber(0, 11));
                intakeCheckbox = By.Id("3SectionT52626-" + Utilities.GetRandNumber(0, 6));
			}
		}

        private void ClickOk()
        {
            ClickOnElement(By.CssSelector("div#OriginalDiv" + testId + " div.functionaltest-calc-total input"));
        }

        public void CompleteTest()
        {
            ClickOnElement(patientSatisfaction);
            ClickOnElement(intakeCheckbox);
            ClickOk();
        }
    }
}
