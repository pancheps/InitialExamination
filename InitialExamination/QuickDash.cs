﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class QuickDash : BasicPageSetup
    {
		private By patientSatistaction = By.Id("1SectionT4151-0");
		private By selectElements;
		private string testId = "T415";

		public QuickDash(IWebDriver mainDriver) : base(mainDriver)
        {
            selectElements = By.CssSelector("select[id^='" + testId + "']");
        }

		private void FillRadios()
		{
			ClickOnElement(patientSatistaction);
			ReadOnlyCollection<IWebElement> inputs = driver.FindElements(selectElements);
			IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
			foreach (var inputEl in inputs)
			{
				js.ExecuteScript("arguments[0].selectedIndex='1'", inputEl);
			}
			driver.FindElement(By.Id(testId + "YN1")).Click();
		}

		private void ClickOk()
		{
			ClickOnElement(By.CssSelector("div#OriginalDiv" + testId + " div.functionaltest-calc-total input"));
		}

		public void CompleteTest()
		{
			FillRadios();
			ClickOk();
		}

	}
}
