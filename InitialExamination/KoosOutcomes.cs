﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class KoosOutcomes : BasicOutcomes
    {
		public KoosOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
		}

		public void FillTest()
		{
            FillTest(42);
            SubmitTest();
		}
	}
}
