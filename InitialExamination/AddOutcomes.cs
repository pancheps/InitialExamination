﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace InitialExamination
{
    public class AddOutcomes : BasicPageSetup
    {
        private By fillOutNow = By.Id("fillOutNowOption");
        private By bottomButtons = By.ClassName("base-default-button-text");
        private By selectOne = By.CssSelector("div.base-default-lightbox-inner-body > select#selectedQuestionnaire");
		private By selectTests = By.Id("selectedQuestionnaire");
        private By recordActions = By.ClassName("ta--patient-chart--actions-select");
        private By submitButton = By.Id("prActionsSubmitButton");
		const string DHI = "DHI";
		const string HOOS = "HOOS";
		const string KOOS = "KOOS";
		const string LEFS = "LEFS";
		const string FALLS = "MFES";
		const string OSWESTRY = "Oswestry";
		const string NDI = "NDI";
		const string UEQD = "Quick DASH";

		public AddOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
            driver.SwitchTo().DefaultContent();
        }

		private void SelectOutcomes()
		{
			SelectPopulatedListByText(recordActions, "Add Outcome");
		}

		private void ClickOnSubmit()
		{
			ClickOnElement(submitButton);
		}

		private void ClickNext()
        {
            ReadOnlyCollection<IWebElement> buttons = driver.FindElements(bottomButtons);
            buttons[buttons.Count - 1].Click();
        }

        private void FillOutNowOption()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("document.getElementById('fillOutNowOption').click();");
        }

        private void FillOutNow()
        {
            FillOutNowOption();
            ClickNext();
        }

        private void SelectATest(string testValue)
        {
            SelectElement sel = new SelectElement(driver.FindElement(selectOne));
            sel.SelectByValue(testValue);
        }

		private void SelectDhi()
		{
            SelectATest(DHI);
			ClickNext();
            DhiOutcomes test = new DhiOutcomes(driver);
            test.FillTest();
		}

		private void SelectHoos()
		{
            SelectATest(HOOS);
            ClickNext();
            HoosOutcomes test = new HoosOutcomes(driver);
            test.FillTest();
		}

		private void SelectKoos()
		{
            SelectATest(KOOS);
            ClickNext();
            KoosOutcomes test = new KoosOutcomes(driver);
            test.FillTest();
		}

		private void SelectLefs()
		{
            SelectATest(LEFS);
            ClickNext();
            LefsOutcomes test = new LefsOutcomes(driver);
            test.FillTest();
		}

		private void SelectMfes()
		{
            SelectATest(FALLS);
            ClickNext();
            MfesOutcomes test = new MfesOutcomes(driver);
            test.FillTest();
		}

		private void SelectOswestry()
		{
            SelectATest(OSWESTRY);
            ClickNext();
            OswestryOutcomes test = new OswestryOutcomes(driver);
            test.FillTest();
		}

		private void SelectNdi()
		{
            SelectATest(NDI);
            ClickNext();
            NdiOutcomes test = new NdiOutcomes(driver);
            test.FillTest();
		}

		private void SelectQuickDash()
		{
			SelectATest(UEQD);
            ClickNext();
            DashOutcomes test = new DashOutcomes(driver);
            test.FillTest();
		}

        private void StartOutcomes()
        {
            SelectOutcomes();
            ClickOnSubmit();
        }

        public void AddOutcomesTests()
        {
            StartOutcomes();
            FillOutNow();
            SelectDhi();

			StartOutcomes();
			FillOutNow();
			SelectHoos();

			StartOutcomes();
			FillOutNow();
			SelectKoos();

            StartOutcomes();
            FillOutNow();
            SelectLefs();

            StartOutcomes();
            FillOutNow();
            SelectMfes();

            StartOutcomes();
            FillOutNow();
            SelectOswestry();

            StartOutcomes();
            FillOutNow();
            SelectNdi();

            StartOutcomes();
            FillOutNow();
            SelectQuickDash();
		}
	}
}
