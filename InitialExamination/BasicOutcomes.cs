﻿using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class BasicOutcomes : BasicPageSetup
    {
		private string originalHandler;
		private By submitButton = By.CssSelector("button[type='submit']");
		private By confirmSubmit = By.Id("ngdialog1");
		private By submitConfirm = By.ClassName("btn-popup");

		public BasicOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
			originalHandler = driver.CurrentWindowHandle;
		}

		private void CheckOpenedWindow()
		{
			while (driver.WindowHandles.Count == MainClass.openWindows) { }
			driver.SwitchTo().Window(driver.WindowHandles[driver.WindowHandles.Count - 1]);
		}

        private int ClickOnRepeatedPosition(int startPos, int rows, int columns = 1, int pos = 0)
		{
			int visibleElems = 0;
			var elems = driver.FindElements(By.ClassName("btn-default"));
			if (startPos < elems.Count)
            {
                for (int i = startPos; visibleElems < rows * columns && i < elems.Count; i++)
				{
					if (elems[i].Displayed)
					{
						if (visibleElems % columns == 0)
    					{
							elems[i + pos].Click();
						}
						visibleElems++;
					}
				}
			}
            else
            {
				var otherElems = driver.FindElements(By.ClassName("content-answer"));
				for (int i = 0; i < otherElems.Count; i++)
				{
					if (otherElems[i].Displayed)
					{
						if (visibleElems % columns == 0)
    					{
							otherElems[i + pos].Click();
						}
						visibleElems++;
					}
				}
			}
			return startPos + rows * columns;
		}

		private void SelectAllOptions(int radios, int options)
		{
			WaitForClickability(submitButton);
            ClickOnRepeatedPosition(ClickOnRepeatedPosition(ClickOnRepeatedPosition(0, 1, 11), 3, 4), radios, options);
		}

		private void SubmitForm()
		{
			ClickOnElement(submitButton);
		}

		private void ConfirmSubmitForm()
		{
			WaitForElementToExist(confirmSubmit);
			var buttons = driver.FindElements(submitConfirm);
			buttons[buttons.Count - 1].Click();
		}

		private void CloseWindow()
		{
			driver.Close();
			driver.SwitchTo().Window(originalHandler);
		}

		public void FillTest(int radios = 0, int options = 5)
		{
			CheckOpenedWindow();
			SelectAllOptions(radios, options);
		}

        public void SubmitTest()
        {
			SubmitForm();
			ConfirmSubmitForm();
			WaitForElementToDisappear(confirmSubmit);
			CloseWindow();
		}
	}
}
