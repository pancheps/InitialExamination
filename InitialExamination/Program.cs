﻿using System;
using OpenQA.Selenium;
using OpenQA.Selenium.Firefox;

namespace InitialExamination
{
    class MainClass
    {
		const string baseUrl = "https://auth.webpt56.vagrant/";
		const int numUsers = 3;
		const string userName = "developer";
		const string password = "password";
        const string ghostUser = "Poncho5";
        public static int openWindows;

		public static void Main(string[] args)
        {
			FirefoxProfile profile = new FirefoxProfile();
			FirefoxOptions options = new FirefoxOptions();
			options.Profile = profile;
			IWebDriver driver = new FirefoxDriver(options);
			driver.Manage().Timeouts().ImplicitWait = TimeSpan.FromSeconds(10);
			driver.Navigate().GoToUrl(baseUrl);
            openWindows = driver.WindowHandles.Count;

            LoginPage lp = new LoginPage(driver);
            lp.Login(userName, password);

            SearchUserPage browse = new SearchUserPage(driver);
            browse.Search(args.Length > 0 ? args[0] : ghostUser);

            SelectUserPage selectedUser = new SelectUserPage(driver);
            selectedUser.SelectUser(args.Length > 0 ? args[0] : ghostUser);

            UserDashboardPage dash = new UserDashboardPage(driver);
            dash.QuickAddNewPatient();

            QuickAddPopup popup = new QuickAddPopup(driver);
            popup.SetData();

            SearchForPatient patient = new SearchForPatient(driver);
            patient.SearchPatient();

            PatientDashboard patinitial = new PatientDashboard(driver);
			/*Initial Examination START*/
			patinitial.StartInitialExam();

            ChangeBasicInfoTab changeDates = new ChangeBasicInfoTab(driver);
            changeDates.ChangeDates();

            ObjectiveQuestionnairesTab test = new ObjectiveQuestionnairesTab(driver);
            test.SelectTest();

            BillingTab billTab = new BillingTab(driver);
            //            billTab.FinalizeInitialExam();
            billTab.BackToPatientRecords();
            /*Initial Examination END*/

            /*Outcomes START*/
            AddOutcomes outcomes = new AddOutcomes(driver);
            outcomes.AddOutcomesTests();
            /*Outcomes END*/

            /*Closing Everything*/
            driver.Quit();
            Environment.Exit(0);
		}
    }
}
