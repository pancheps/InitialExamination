﻿using System;
using System.Threading;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class ChangeBasicInfoTab : BasicPageSetup
    {
        private By testsIframe = By.Id("soap-iframe");
        private By mainForm = By.Id("addSOAP");
        private By dateDay = By.Id("DateDay");
        private By injuryDay = By.Id("InjuryDateDay");

        public ChangeBasicInfoTab(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        private void ChangeDateToFirst()
        {
            SelectPopulatedListByValue(dateDay, "01");
        }

        private void ChangeInjuryDateToFirst()
        {
            SelectPopulatedListByValue(injuryDay, "01");
        }

        public void ChangeDates()
        {
            
            WaitForVisibility(testsIframe);
            driver.SwitchTo().Frame(driver.FindElement(testsIframe));
            ChangeDateToFirst();
            ChangeInjuryDateToFirst();
        }
    }
}
