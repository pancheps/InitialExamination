﻿using System;
using System.Collections.ObjectModel;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class ModifiedFalls : BasicPageSetup
    {
		private By patientSatistaction = By.Id("1SectionT7511-0");
		private By inputElements;
		private string testId = "T751";

        public ModifiedFalls(IWebDriver mainDriver) : base(mainDriver)
        {
			inputElements = By.CssSelector("input[id^='" + testId + "'][value='0']");
		}

		private void FillRadios()
		{
			ClickOnElement(patientSatistaction);
			ReadOnlyCollection<IWebElement> inputs = driver.FindElements(inputElements);
			foreach (var inputEl in inputs)
			{
				inputEl.Click();
			}
			driver.FindElement(By.Id(testId + "010")).Click();
			driver.FindElement(By.Id(testId + "020")).Click();
			driver.FindElement(By.Id(testId + "030")).Click();
			driver.FindElement(By.Id(testId + "040")).Click();
			driver.FindElement(By.Id(testId + "YN1")).Click();
		}

		private void ClickOk()
		{
			ClickOnElement(By.CssSelector("div#OriginalDiv" + testId + " div.functionaltest-calc-total input"));
		}

		public void CompleteTest()
		{
			FillRadios();
			ClickOk();
		}

	}
}
