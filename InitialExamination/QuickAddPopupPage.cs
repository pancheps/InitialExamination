﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace InitialExamination
{
    public class QuickAddPopup : BasicPageSetup
    {
        private By firstName = By.Name("FirstName");
        private By lastName = By.Name("LastName");
        private By gender = By.Name("Sex");
        private By month = By.Name("DOBMonth");
        private By day = By.Name("DOBDay");
        private By year = By.Name("DOBYear");
        private By phoneAreaCode = By.Id("Teleb_TelephoneAreaCode");
        private By phonePrefix = By.Name("Number1");
        private By phoneSuffix = By.Name("Number2");
        private By addButton = By.Id("btn-quick-add");
		private By insurance = By.Id("InsID");
        private By selectedInsurance = By.ClassName("x-combo-selected");
		private By physician = By.Id("PhysicianID");
        private By icd = By.LinkText("Add new code");
		private FileAccess config;
        private string originalHandler;

        public QuickAddPopup(IWebDriver mainDriver, string path = null) : base(mainDriver)
        {
            config = new FileAccess(path);
            originalHandler = driver.CurrentWindowHandle;
        }

		private void SetFirstName(string firstNameText)
		{
            CompleteText(firstName, firstNameText);
		}

		private void SetLastName(string lastNameText)
		{
            CompleteText(lastName, lastNameText);
		}

		private void SetGender(string genderText)
		{
            CompleteText(gender, genderText);
            SelectOnlyItemOnList(gender);
		}

		private void SetMonth(string monthText)
		{
            CompleteText(month, monthText);
		}

		private void SetDay(string dayText)
		{
            CompleteText(day, dayText);
		}

		private void SetYear(string yearText)
		{
            CompleteText(year, yearText);
		}

		private void SetPhoneArea(string phoneAreaText)
		{
            CompleteText(phoneAreaCode, phoneAreaText);
		}

		private void SetPhonePrefix(string phonePrefixText)
		{
            CompleteText(phonePrefix, phonePrefixText);
		}

		private void SetPhoneSuffix(string phoneSuffixText)
		{
            CompleteText(phoneSuffix, phoneSuffixText);
		}

		private void SetInsurance(string insuranceText)
		{
            CompleteText(insurance, insuranceText);
            Thread.Sleep(2000);
            SelectOnlyItemOnList(insurance);
		}

		private void SetPhysician(string physicianText)
		{
            CompleteText(physician, physicianText);
			Thread.Sleep(2000);
            SelectOnlyItemOnList(physician);
		}

        private void SetIcdCode(string codeText)
        {
            ClickOnElement(icd);

            NewIcdCodeSubPopup icdCode = new NewIcdCodeSubPopup(driver, codeText);
            icdCode.AddCode();
		}

		private void ClickAddPatient()
        {
            driver.FindElement(addButton).Click();
        }

		private void SwitchToPopup()
		{
			var openWindows = driver.WindowHandles;
            driver.SwitchTo().Window(openWindows[openWindows.Count - 1]);
		}

		private void SwitchToMain()
		{
            driver.SwitchTo().Window(originalHandler);
		}

        public void SetData(int? i = null)
        {
            string[] patientData = config.GetValues();
            SwitchToPopup();
            SetFirstName(i + Utilities.GetOneItem(patientData[0]));
			SetLastName(Utilities.GetOneItem(patientData[1]));
			SetGender(Utilities.GetOneItem(patientData[2]));
			SetMonth(Utilities.GetOneItem(patientData[3]));
			SetDay(Utilities.GetOneItem(patientData[4]));
			SetYear(Utilities.GetOneItem(patientData[5]));
			SetPhoneArea(Utilities.GetOneItem(patientData[6]));
			SetPhonePrefix(Utilities.GetOneItem(patientData[7]));
			SetPhoneSuffix(Utilities.GetOneItem(patientData[8]));
            SetInsurance(Utilities.GetOneItem(patientData[9]));
            SetPhysician(Utilities.GetOneItem(patientData[10]));
            SetIcdCode(Utilities.GetOneItem(patientData[11]));
			ClickAddPatient();
            if (CheckDuplicatePatient())
            {
                driver.Close();
            }
            SwitchToMain();
		}

        private bool CheckDuplicatePatient()
        {
            try
            {
                WebDriverWait waitPopupClose = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
				waitPopupClose.Until(driver => (driver.WindowHandles.Count == MainClass.openWindows));
            }
            catch(Exception)
            {
                return true;
            }
            return false;
        }
	}
}
