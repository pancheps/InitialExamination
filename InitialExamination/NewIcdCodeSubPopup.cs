﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class NewIcdCodeSubPopup : BasicPageSetup
    {
		private By icdField = By.ClassName("icd-search--search-query");
        private By searchButton = By.ClassName("icd-builder--button-search");
        private By icdButton = By.ClassName("icd-tabbing-button");
        private By icdButtonAdd = By.ClassName("icd-builder--button-add");
		private string icdCode;

        public NewIcdCodeSubPopup(IWebDriver mainDriver, string code) : base(mainDriver)
        {
            icdCode = code;
        }

        private void AddIcdCode()
        {
            CompleteText(icdField, icdCode);
        }

        private void ClickOnSearch()
        {
            ClickOnElement(searchButton);
        }

        private void ClickOnSearchResult()
        {
            ClickOnElement(icdButton);
        }

        private void ClickOnAddIcdCode()
        {
            ClickOnElement(icdButtonAdd);
        }

        public void AddCode()
        {
            AddIcdCode();
            ClickOnSearch();
            WaitForVisibility(icdButton);
            ClickOnSearchResult();
            WaitForVisibility(icdButtonAdd);
            ClickOnAddIcdCode();
        }
    }
}
