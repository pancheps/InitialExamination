﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class HoosOutcomes : BasicOutcomes
    {
		public HoosOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
		}

        public void FillTest()
        {
            FillTest(40);
            SubmitTest();
        }
	}
}
