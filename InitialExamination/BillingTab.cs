﻿using System;
using System.Threading;
using OpenQA.Selenium;
using OpenQA.Selenium.Support.UI;

namespace InitialExamination
{
    public class BillingTab : BasicPageSetup
    {
        private By billingTab = By.LinkText("Billing");
        private By finalizeButton = By.ClassName("ta--soap--finalize");
        private By confirmButton = By.ClassName("base-default-button-text");
        private By backToPatient = By.LinkText("Back to Patient Records");

        public BillingTab(IWebDriver mainDriver) : base(mainDriver)
        {
            ClickOnElement(billingTab);
            Thread.Sleep(2000);
        }

        private void ClickOnFinalize()
        {
            IJavaScriptExecutor js = (IJavaScriptExecutor)driver;
            js.ExecuteScript("SoapObj.SaveForm('PFB2', 'finalize', 'Finalize Document')");
        }

        private void ClickOnConfirm()
        {
            WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
            wait.Until(ExpectedConditions.AlertIsPresent());
            IAlert alert = driver.SwitchTo().Alert();
            alert.Accept();
        }

        private void ClickOnConfirmPopup()
        {
            ClickOnFirstCollectionElement(confirmButton);
        }

        public void BackToPatientRecords()
        {
            ClickOnElement(backToPatient);
			WebDriverWait wait = new WebDriverWait(driver, TimeSpan.FromSeconds(5));
			wait.Until(ExpectedConditions.AlertIsPresent());
			IAlert alert = driver.SwitchTo().Alert();
			alert.Accept();
		}

        public void FinalizeInitialExam()
        {
            ClickOnFinalize();
            //if (ExpectedConditions.AlertIsPresent().Invoke(driver) != null)
            //{
                ClickOnConfirm();
 //               Thread.Sleep(3000);
            //}
            if (driver.FindElements(confirmButton).Count != 0)
            {
				ClickOnConfirmPopup();
			}
        }
    }
}
