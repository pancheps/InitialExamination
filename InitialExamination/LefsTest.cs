﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class LefsTest : BasicPageSetup
    {
		private By patientSatistaction = By.Id("1SectionT3771-0");
		private string testId = "T377";

		public LefsTest(IWebDriver mainDriver) : base(mainDriver)
        {
        }

		private void FillRadios()
		{
			ClickOnElement(patientSatistaction);
		}

		private void ClickOk()
        {
            ClickOnElement(By.CssSelector("div#OriginalDiv" + testId + " div.functionaltest-calc-total input"));
        }

        public void CompleteTest()
        {
            FillRadios();
            ClickOk();
        }

    }
}
