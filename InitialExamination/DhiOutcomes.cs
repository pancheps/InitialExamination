﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class DhiOutcomes : BasicOutcomes
    {
        private By checkBoxesLabel;

        public DhiOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
            checkBoxesLabel = By.CssSelector("label[class='radio-custom-label'][for^='0']");
        }

        private void ClickOnCheckRadios(int elem = 0)
        {
            var elems = driver.FindElements(By.ClassName("radio-custom-label"));
            elems[elem].Click();
        }

        public void FillTest()
        {
            FillTest(25, 3);
            ClickOnCheckRadios();
            SubmitTest();
        }
    }
}
