﻿using System;
using OpenQA.Selenium;

namespace InitialExamination
{
    public class UserDashboardPage : BasicPageSetup
    {
        private By modalButton = By.ClassName("base-default-button-text");
        private By addPatientLink = By.LinkText("Quick Add Patient");

        public UserDashboardPage(IWebDriver mainDriver) : base(mainDriver)
        {
			if (driver.FindElements(modalButton).Count > 0)
			{
				GettingRidOfModal();
			}
		}

        private void GettingRidOfModal()
        {
            ClickOnElement(modalButton);
        }

        private void ClickQuickAddPatientLink()
        {
            ClickOnElement(addPatientLink);
        }

        public void QuickAddNewPatient()
        {
            ClickQuickAddPatientLink();
        }
    }
}
