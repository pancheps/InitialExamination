﻿using OpenQA.Selenium;

namespace InitialExamination
{
    public class NdiOutcomes : BasicOutcomes
    {
        public NdiOutcomes(IWebDriver mainDriver) : base(mainDriver)
        {
        }

        public void FillTest()
        {
            FillTest(10, 6);
            SubmitTest();
        }
    }
}
